Changelog

Todos los cambios notables a este proyecto serán documentados es este archivo.

El formato esta basado en [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), mantenemos las versiones de acuerdo
a [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

**Dada una versión número MAJOR.MINOR.PATCH:** (aplica a partir de 2.0.0)

* Se incrementará **MAJOR**, cuando se implemente un nuevo tipo de **Módulo** que genere incompatibilidad con versión
  anterior.
* Se incrementa **MINOR**, cuando se agregue una o más **nueva(s) funcionalidad(es) a la aplicación**.
* Se incrementa **PATCH**, cuando se implemente un **fix**.



## [Released]

## [1.0.0] - 2022-11-18
### [merge]
* [develop](https://ejemplomerge.com) - Se hace merge de la rama develop a master

## [0.1.1] - 2022-11-18
### [fix]
* [BUG-GITFLOW-02](https://ejemploBug.com) - Se resolvio problema de un bug en el README

## [0.1.0] - 2022-18-11
### [add]
* [HU-GITFLOW-01](https://ejemploHU.com) - add: Se agrego el archivo index.html
